
const paragraphs = document.querySelectorAll('p');
paragraphs.forEach(p => {
  p.style.backgroundColor = '#ff0000';
});

const optionsList = document.getElementById('optionsList');
console.log(optionsList);
console.log(optionsList.parentNode);

 if (optionsList.hasChildNodes()) {
  optionsList.childNodes.forEach(childNode => {
    console.log(childNode.nodeName, childNode.nodeType);
  });
}

// 3 

// -

const mainHeader = document.querySelector('.main-header');
const navItems = mainHeader.children;

for (let i = 0; i < navItems.length; i++) {
  navItems[i].classList.add('nav-item');
  console.log(navItems[i]);
}
const sectionTitles = document.querySelectorAll('.section-title');
sectionTitles.forEach(title => {
  title.classList.remove('section-title');
});